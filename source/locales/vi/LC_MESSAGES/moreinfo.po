# Vietnamese translation for Release Notes (More Info).
# Copyright © 2009 Free Software Foundation, Inc.
# Clytie Siddall <clytie@riverland.net.au>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 5.0\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2023-11-05 13:18+0100\n"
"PO-Revision-Date: 2009-02-04 17:56+1030\n"
"Last-Translator: Clytie Siddall <clytie@riverland.net.au>\n"
"Language: vi\n"
"Language-Team: Vietnamese <vi-VN@googlegroups.com>\n"
"Plural-Forms: nplurals=1; plural=0\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: ../moreinfo.rst:4
msgid "More information on Debian"
msgstr "Thông tin thêm về Debian"

#: ../moreinfo.rst:9
msgid "Further reading"
msgstr "Đọc thêm"

#: ../moreinfo.rst:11
#, fuzzy
msgid ""
"Beyond these release notes and the installation guide (at |URL-INSTALLER-"
"MANUAL|) further documentation on Debian is available from the Debian "
"Documentation Project (DDP), whose goal is to create high-quality "
"documentation for Debian users and developers, such as the Debian "
"Reference, Debian New Maintainers Guide, the Debian FAQ, and many more. "
"For full details of the existing resources see the `Debian Documentation "
"website <https://www.debian.org/doc/>`__ and the `Debian Wiki "
"<https://wiki.debian.org/>`__."
msgstr ""
"Ra khỏi Ghi chú Phát hành này và Sổ tay Cài đặt, tài liệu hướng dẫn về "
"Debian sẵn sàng từ Dự án Tài liệu Debian (Debian Documentation Project: "
"DDP) mà có mục đích tạo tài liệu hướng dẫn có chất lượng cao cho các "
"người dùng và nhà phát triển Debian. Tài liệu hướng dẫn, bao gồm Debian "
"Reference (Tham chiếu Debian), Debian New Maintainers Guide (Sổ tay Nhà "
"duy trì Mới) và Debian FAQ (Hỏi Đáp) cũng sẵn sàng (tiếc là chưa dịch "
"sang tiếng Việt) và rất nhiều tài liệu hữu ích bổ sung. Để tìm chi tiết "
"về những tài nguyên sẵn sàng, xem `Địa chỉ Web của DDP "
"<https://www.debian.org/doc/>`__."

# | msgid ""
# | "Documentation for individual packages is installed into ``/usr/"
# | "share/doc/<replaceable>package</replaceable>``.  This may "
# | "include copyright information, Debian specific details and any upstream "
# | "documentation."
#: ../moreinfo.rst:20
#, fuzzy
msgid ""
"Documentation for individual packages is installed into "
"``/usr/share/doc/package``. This may include copyright information, "
"Debian specific details, and any upstream documentation."
msgstr ""
"Tài liệu hướng dẫn về mỗi gói riêng được cài đặt vào thư mục "
"``/usr/share/doc/tên_gói``. Các tài liệu này có thể bao gồm thông tin tác"
" quyền, chi tiết đặc trưng cho Debian, và tài liệu hướng dẫn của nhà phát"
" triển gốc. "

#: ../moreinfo.rst:27
msgid "Getting help"
msgstr "Tìm trợ giúp"

# | msgid ""
# | "There are many sources of help, advice and support for Debian users, but
# "
# | "these should only be considered if research into documentation of the "
# | "issue has exhausted all sources.  This section provides a short "
# | "introduction into these which may be helpful for new Debian users."
#: ../moreinfo.rst:29
#, fuzzy
msgid ""
"There are many sources of help, advice, and support for Debian users, "
"though these should only be considered after researching the issue in "
"available documentation. This section provides a short introduction to "
"these sources which may be helpful for new Debian users."
msgstr ""
"Có rất nhiều nguồn trợ giúp, lời khuyên và hỗ trợ cho người dùng Debian, "
"nhưng chỉ nên xem xét nguồn như vậy nếu tài liệu hướng dẫn có sẵn không "
"giúp người dùng giải quyết vấn đề. Phần này giới thiệu ngắn những nguồn "
"trợ giúp bổ sung có thể hữu ích cho người dùng Debian vẫn bắt đầu."

#: ../moreinfo.rst:37
msgid "Mailing lists"
msgstr "Hộp thư chung"

#: ../moreinfo.rst:39
msgid ""
"The mailing lists of most interest to Debian users are the debian-user "
"list (English) and other debian-user-language lists (for other "
"languages). For information on these lists and details of how to "
"subscribe see `<https://lists.debian.org/>`__. Please check the archives "
"for answers to your question prior to posting and also adhere to standard"
" list etiquette."
msgstr ""
"Những hộp thư chung (mailing list) hữu ích nhất cho người dùng Debian là "
"« debian-user » (tiếng Anh: cũng có thể yêu cầu dự án tạo một hộp thư "
"chung người dùng bằng ngôn ngữ mẹ đẻ). Để tìm thông tin về các hộp thư "
"chung Debian và cách đăng ký, xem `<https://lists.debian.org/>`__. Trước "
"khi gửi một thư xin trợ giúp, người dùng nên tìm kiếm qua kho thư (mà "
"chứa rất nhiều thông tin về vấn đề thường gặp), và tùy theo quy ước mặc "
"nhận trong hộp thư chung (v.d. hãy lặng lẽ, viết đáp ứng bên dưới thân "
"thư gốc, cắt ngắn đoạn văn gốc không còn có ích lại, không nên VIẾT CẢ "
"CHỮ HOA, không đính kèm tập tin (gửi tập tin đính kèm một thư riêng cho "
"người dùng khác), thay đổi dòng Chủ đề khi giới thiệu một vấn đề mới)."

#: ../moreinfo.rst:49
msgid "Internet Relay Chat"
msgstr "IRC — Trò chuyện chuyển tiếp trên Internet"

# | msgid ""
# | "Debian has an IRC channel dedicated to the support and aid of Debian "
# | "users located on the OFTC IRC network.  To access the channel, point your
# "
# | "favorite IRC client at irc.debian.org and join
# ``#debian``."
#: ../moreinfo.rst:51
#, fuzzy
msgid ""
"Debian has an IRC channel dedicated to support and aid for Debian users, "
"located on the OFTC IRC network. To access the channel, point your "
"favorite IRC client at irc.debian.org and join ``#debian``."
msgstr ""
"Debian có một kênh IRC dành cho hỗ trợ và giúp đỡ các người dùng Debian, "
"chạy trên mạng IRC OFTC. Để truy cập đến kênh này, hãy chỉ ứng dụng khách"
" IRC (v.d. XChat) tới địa chỉ « irc.debian.org » và vào kênh « "
"``#debian`` »."

# | msgid ""
# | "Please follow the channel guidelines, respecting other users fully.  The
# "
# | "guidelines are available at the <ulink url=\"&url-wiki;DebianIRC\">Debian
# | "Wiki</ulink>."
#: ../moreinfo.rst:55
#, fuzzy
msgid ""
"Please follow the channel guidelines, respecting other users fully. The "
"guidelines are available at the `Debian Wiki "
"<https://wiki.debian.org/DebianIRC>`__."
msgstr ""
"Hãy tùy theo các hướng dẫn về sử dụng kênh (quan trọng nhất là lặng lẽ) "
"mà sẵn sàng trong `Debian Wiki <https://wiki.debian.org/DebianIRC>`__."

#: ../moreinfo.rst:58
msgid ""
"For more information on OFTC please visit the `website "
"<http://www.oftc.net/>`__."
msgstr ""
"Để tìm thêm thông tin về mạng OFTC, xem `địa chỉ Web "
"<http://www.oftc.net/>`__."

#: ../moreinfo.rst:64
msgid "Reporting bugs"
msgstr "Thông báo lỗi"

#: ../moreinfo.rst:66
#, fuzzy
msgid ""
"We strive to make Debian a high-quality operating system; however that "
"does not mean that the packages we provide are totally free of bugs. "
"Consistent with Debian's \"open development\" philosophy and as a service"
" to our users, we provide all the information on reported bugs at our own"
" Bug Tracking System (BTS). The BTS can be browsed at "
"`<https://bugs.debian.org/>`__."
msgstr ""
"Chúng tôi cố gắng làm cho Debian là một hệ điều hành có chất lượng cao, "
"tuy nhiên đó không có nghĩa là mọi gói đều hoàn toàn miễn lỗi. Tùy theo ý"
" kiến \"phát triển mở\" của dự án Debian, và để giúp đỡ các người dùng, "
"chúng tôi cung cấp tất cả các thông tin về những lỗi đã thông báo, thông "
"qua Hệ thống Theo dõi Lỗi (BTS): có thể duyệt qua nó ở "
"`<https://bugs.debian.org/>`__."

# | msgid ""
# | "If you find a bug in the distribution or in packaged software that is "
# | "part of it, please report it so that it can be properly fixed for future
# "
# | "releases.  Reporting bugs requires a valid email address.  We ask for "
# | "this so that we can trace bugs and developers can get in contact with "
# | "submitters should additional information be needed."
#: ../moreinfo.rst:72
#, fuzzy
msgid ""
"If you find a bug in the distribution or in packaged software that is "
"part of it, please report it so that it can be properly fixed for future "
"releases. Reporting bugs requires a valid e-mail address. We ask for this"
" so that we can trace bugs and developers can get in contact with "
"submitters should additional information be needed."
msgstr ""
"Nếu người dùng gặp một lỗi trong bản phân phối, hoặc trong phần mềm đã "
"đóng gói thuộc về nó, hãy thông báo nó để cho phép nhà phát triển sửa "
"chữa trong bản phát hành sau. Để thông báo lỗi, cũng cần nhập một địa chỉ"
" thư điện tử vẫn hoạt động. Địa chỉ này cần thiết để giúp nhà phát triển "
"yêu cầu thêm thông tin, và theo dõi mẫu của các lỗi hiện thời."

#: ../moreinfo.rst:78
#, fuzzy
msgid ""
"You can submit a bug report using the program ``reportbug`` or manually "
"using e-mail. You can find out more about the Bug Tracking System and how"
" to use it by reading the reference documentation (available at "
"``/usr/share/doc/debian`` if you have **doc-debian** installed) or online"
" at the `Bug Tracking System <https://bugs.debian.org/>`__."
msgstr ""
"Người dùng lúc nào cũng có khả năng tự động gửi báo cáo lỗi dùng chương "
"trình ``reportbug`` hoặc tự gửi một báo cáo bằng thư điện tử. Có thêm "
"thông tin về Hệ thống Theo dõi Lỗi và cách sử dụng nó trong tài liệu tham"
" chiếu (có sẵn trong thư mục ``/usr/share/doc/debian`` nếu người dùng đã "
"cài đặt gói **doc-debian**) hoặc trên Internet ở `Hệ thống Theo dõi Lỗi "
"<https://bugs.debian.org/>`__."

#: ../moreinfo.rst:87
msgid "Contributing to Debian"
msgstr "Đóng góp cho Debian"

#: ../moreinfo.rst:89
#, fuzzy
msgid ""
"You do not need to be an expert to contribute to Debian. By assisting "
"users with problems on the various user support `lists "
"<https://lists.debian.org/>`__ you are contributing to the community. "
"Identifying (and also solving) problems related to the development of the"
" distribution by participating on the development `lists "
"<https://lists.debian.org/>`__ is also extremely helpful. To maintain "
"Debian's high-quality distribution, `submit bugs "
"<https://bugs.debian.org/>`__ and help developers track them down and fix"
" them. The tool ``how-can-i-help`` helps you to find suitable reported "
"bugs to work on. If you have a way with words then you may want to "
"contribute more actively by helping to write `documentation "
"<https://www.debian.org/doc/vcs>`__ or `translating "
"<https://www.debian.org/international/>`__ existing documentation into "
"your own language."
msgstr ""
"Để đóng góp cho Debian, người dùng không cần là chuyên gia về vi tính. "
"Cũng có thể đóng góp cho cộng đồng bằng cách giúp người dùng xin trợ giúp"
" trong các `hộp thư chung <https://lists.debian.org/>`__ hỗ trợ người "
"dùng. Trong `hộp thư chung <https://lists.debian.org/>`__ kiểu phát "
"triển, mỗi người tham gia cũng rất giúp ích. Bản phân phối Debian chỉ bảo"
" tồn mức chất lượng khi người dùng `thông báo lỗi "
"<https://bugs.debian.org/>`__ được phát hiện, ngay cả giúp nhà phát triển"
" tìm vết và sửa chữa lỗi. Cũng có thể giúp viết `tài liệu "
"<https://www.debian.org/doc/vcs>`__  gốc hoặc `dịch "
"<https://www.debian.org/international/>`__ tài liệu đã tồn tại sang tiếng"
" Việt (tham gia nhé :) )."

#: ../moreinfo.rst:103
#, fuzzy
msgid ""
"If you can dedicate more time, you could manage a piece of the Free "
"Software collection within Debian. Especially helpful is if people adopt "
"or maintain items that people have requested for inclusion within Debian."
" The `Work Needing and Prospective Packages database "
"<https://www.debian.org/devel/wnpp/>`__ details this information. If you "
"have an interest in specific groups then you may find enjoyment in "
"contributing to some of Debian's `subprojects "
"<https://www.debian.org/devel/#projects>`__ which include ports to "
"particular architectures and `Debian Pure Blends "
"<https://wiki.debian.org/DebianPureBlends>`__ for specific user groups, "
"among many others."
msgstr ""
"Nếu người dùng có đủ thời gian rảnh, cũng có thể quản lý một hay vài gói "
"phần mềm tự do bên trong Debian. Đặc biệt giúp ích khi một người tham gia"
" cũng chịu trách nhiệm về hay duy trì phần mềm được người dùng yêu cầu "
"bao gồm trong Debian. `Cơ sở dữ liệu Việc cần làm và Gói tương lai "
"<https://www.debian.org/devel/wnpp/>`__ (Work Needing and Prospective "
"Packages) chứa thông tin đó. Người dùng quan tâm đến một vùng riêng cũng "
"có thể đóng góp cho dự án con của Debian, v.d. chuyển phần mềm sang kiến "
"trúc riêng, <ulink url=\"&url-debian-jr;\">Debian Trẻ</ulink> và <ulink "
"url=\"&url-debian-med;\">Y tế Debian</ulink>."

# | msgid ""
# | "In any case, if you are working in the free software community in any "
# | "way, as a user, programmer, writer or translator you are already helping
# "
# | "the free software effort.  Contributing is rewarding and fun, and as well
# | "as allowing you to meet new people it gives you that warm fuzzy feeling "
# | "inside."
#: ../moreinfo.rst:114
#, fuzzy
msgid ""
"In any case, if you are working in the free software community in any "
"way, as a user, programmer, writer, or translator you are already helping"
" the free software effort. Contributing is rewarding and fun, and as well"
" as allowing you to meet new people it gives you that warm fuzzy feeling "
"inside."
msgstr ""
"Trong mọi trường hợp đều, nếu người dùng tham gia cộng đồng phần mềm tự "
"do bằng cách nào cả, là một người dùng, nhà phát triển, tác giả hay người"
" dịch, thì vẫn còn đóng góp cho sự cố gắng phần mềm tự do. Đóng góp vẫn "
"đáng làm và thứ vị, cũng cho phép người dùng kết bạn và học hiểu thêm."

