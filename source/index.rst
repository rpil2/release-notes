Release Notes for Debian |RELEASE| (|RELEASENAME|\)
********************************************************************************************************************************

The Debian Documentation Project `<https://www.debian.org/doc> <https://www.debian.org/doc>`__.

This document is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

The license text can also be found at
`<https://www.gnu.org/licenses/gpl-2.0.html>`__
and ``/usr/share/common-licenses/GPL-2`` on Debian systems.

.. toctree::
   :maxdepth: 3
   :numbered:

   about
   whats-new
   installing
   upgrading
   issues
   moreinfo
   old-stuff
   contributors

.. Editors of this document:
   Steve Langasek <email>vorlon@debian.org</email>
   W. Martin Borgert <email>debacle@debian.org</email>
   Javier Fernandez-Sanguino <email>jfs@debian.org</email>
   Julien Cristau <email>jcristau@debian.org</email>
   and many more!
   See chapter contributors.

